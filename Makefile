AWS_REGION := eu-central-1
PROJECT_NAME := metabase-web-bi
PROJECT_SLUG := metabase
PROJECT_VERSION := 105
AWS_PROFILE := olmax
MB_VERSION := v0.34.0
DEPLOY_STAGE:= staging

define message3
 Environment variable BASE_IP is required. Not set.
	Use following command:
        "$$ my_ip=`curl ipinfo.io | jq .ip`;eval my_ip=$${my_ip[i]};my_ip="$$my_ip/32"; export BASE_IP=$$my_ip"

endef

define message2
 Environment variable ADMIN_EMAIL is required. Not set.
	Use following command:
        "export ADMIN_EMAIL=admin@example.com"

endef

BASE := $(shell /bin/pwd)
CODE_COVERAGE = 72
PIPENV ?= pipenv

#################
#  Python vars	#
#################

# The service is the Actual Function Ref
SERVICE ?= service_not_defined
# Path to the event.json file
EVENT ?= event_not_defined
# Path to sensor template
TEMPLATE ?= template_not_defined


#############
#  SAM vars	#
#############

NETWORK = ""

target:
	$(info ${HELP_MESSAGE})
	@exit 0

clean: ##=> Deletes current build environment and latest build
	$(info [*] Who needs all that anyway? Destroying environment....)
	rm -rf ./${SERVICE}/build
	rm -rf ./${SERVICE}.zip

all: clean build

install: _install_packages _install_dev_packages

shell:
	@$(PIPENV) shell

package: _check_service_definition ##=> Builds package using Docker Lambda container
ifeq ($(DOCKER),1)
	$(info [*] Cleaning up local dev/builds before build task...)
	@$(MAKE) clean SERVICE="${SERVICE}"
	$(info [+] Packaging service '$(SERVICE)' using Docker Lambda -- This may take a while...)
	docker run -v $$PWD:/var/task -it lambci/lambda:build-python3.7 /bin/bash -c 'make _package SERVICE="${SERVICE}"'
else
	$(info [*] Cleaning up local builds before build task...)
	@$(MAKE) clean SERVICE="${SERVICE}"
	$(info [+] Packaging service '$(SERVICE)' -- This may take a while...)
	@$(MAKE) _package SERVICE="${SERVICE}"
endif

build: _check_service_definition _clone_service_to_build ##=> Same as package except that we don't create a ZIP
	@$(MAKE) _install_deps SERVICE="${SERVICE}"


ifndef BASE_IP
export message3
$(error $(message3))
endif

ifndef ADMIN_EMAIL
export message2
$(error $(message2))
endif


CURRENT_LOCAL_IP = $(BASE_IP)
NOTIFY_EMAIL = $(ADMIN_EMAIL)

target: help
	$(info ${HELP_MESSAGE})
	@exit 0

help:
	@grep '^.PHONY:.*' Makefile | sed 's/\.PHONY:[ \t]\+\(.*\)[ \t]\+##[ \t]*\(.*\)/\1	\2/' | expand -t20

.PHONY: vpn ## Unpack vpn config locally
vpn:
	aws s3 cp --profile ${AWS_PROFILE} s3://${PROJECT_NAME}-${PROJECT_VERSION}-dev-vpncerts-${AWS_REGION}/client certs --recursive --exclude="*" --include="*.zip"
	cd certs; unzip metabaseVPNClient.zip
	nmcli con import type openvpn file certs/metabase_vpn_clientuser.ovpn

.PHONY: bundle ## Create zip from initial source bundle
bundle:
	cd metabase-aws-eb-${MB_VERSION}; zip -r ../sourcebundle/${MB_VERSION}/metabase-aws-eb.zip * .ebextensions/

.PHONY: templates ## Sync Cfn templates with Artifacts Bucket
templates: bundle
	# Create lambda artifacts and transform serverless into cloudformation
	cd cloudformation/staging/services/ && sam package --profile ${AWS_PROFILE} --template-file serverless.staging.services.postgres.yml --output-template-file cloudformation.staging.services.postgres.yml --s3-bucket ${PROJECT_NAME}-${PROJECT_VERSION}-dev-cfn-${AWS_REGION} --s3-prefix ${PROJECT_SLUG}-${PROJECT_VERSION}/staging/artifacts
	# Sync cfn templates
	aws s3 cp --profile ${AWS_PROFILE} cloudformation/staging/cluster s3://${PROJECT_NAME}-${PROJECT_VERSION}-dev-cfn-${AWS_REGION}/${PROJECT_SLUG}-${PROJECT_VERSION}/staging/templates/ --recursive --exclude="*" --include="*.yml"
	aws s3 cp --profile ${AWS_PROFILE} cloudformation/staging/services s3://${PROJECT_NAME}-${PROJECT_VERSION}-dev-cfn-${AWS_REGION}/${PROJECT_SLUG}-${PROJECT_VERSION}/staging/templates/ --recursive --exclude="*" --include="*.yml"
	# Sync source bundle
	aws s3 cp --profile ${AWS_PROFILE} sourcebundle s3://${PROJECT_NAME}-${PROJECT_VERSION}-dev-cfn-${AWS_REGION}/${PROJECT_SLUG}-${PROJECT_VERSION}/staging/artifacts/ --recursive --exclude="*" --include="*.zip"

.PHONY: cluster ## Launch the metabase stack - ensure parameters are set correctly in Makefile
cluster: templates
	aws cloudformation --region ${AWS_REGION} --profile ${AWS_PROFILE} create-stack --stack-name ${PROJECT_NAME}-${PROJECT_VERSION} \
	--template-body file://cloudformation/staging/cloudformation.staging.master.metabase.yml \
	--parameters ParameterKey="ProjectSlug",ParameterValue="${PROJECT_SLUG}-${PROJECT_VERSION}" \
	ParameterKey="QSS3KeyPrefix",ParameterValue="staging/" \
	ParameterKey="QSS3BucketName",ParameterValue="${PROJECT_NAME}-${PROJECT_VERSION}-dev-cfn-${AWS_REGION}" \
	ParameterKey="VPNCertsBucketName",ParameterValue="${PROJECT_NAME}-${PROJECT_VERSION}-dev-vpncerts-${AWS_REGION}" \
	ParameterKey="VpnAccessKey",ParameterValue="${PROJECT_SLUG}-${PROJECT_VERSION}-ec2-access-key" \
	ParameterKey="LocalBaseIp",ParameterValue="${CURRENT_LOCAL_IP}" \
	ParameterKey="DatabaseAdminUser",ParameterValue="metabase" \
	ParameterKey="WebInstanceType",ParameterValue="t2.medium" \
	ParameterKey="WebAsgMin",ParameterValue="2" \
	ParameterKey="WebAsgMax",ParameterValue="4" \
	ParameterKey="KeyName",ParameterValue="${PROJECT_SLUG}-${PROJECT_VERSION}-ec2-access-key" \
	ParameterKey="MetabaseVersion",ParameterValue="${MB_VERSION}" \
	ParameterKey="AdminEmail",ParameterValue="${NOTIFY_EMAIL}" \
	ParameterKey="EnvironmentStage",ParameterValue="${DEPLOY_STAGE}" \
	--capabilities CAPABILITY_NAMED_IAM \
	CAPABILITY_AUTO_EXPAND


#############
#  Helpers  #
#############

_install_packages:
	$(info [*] Install required packages...)
	@$(PIPENV) install

_install_dev_packages:
	$(info [*] Install required dev-packages...)
	@$(PIPENV) install -d

_check_service_definition:
	$(info [*] Checking whether service $(SERVICE) exists...)

# SERVICE="<name_of_service>" must be passed as ARG for target or else fail
ifndef SERVICE
	$(error [!] SERVICE env not defined...FAIL)
endif

ifeq ($(wildcard $(SERVICE)/.),)
	$(error [!] '$(SERVICE)' folder doesn't exist)
endif

ifeq ($(wildcard Pipfile),)
	$(error [!] Pipfile dependencies file missing from $(BASE) folder...)
endif

_check_event_definition:
	$(info [*] Checking whether event $(EVENT) exists...)

# EVENT="<name_and_path_of_event>" must be passed as ARG for target or else fail
ifndef EVENT
	$(error [!] EVENT env not defined...FAIL)
endif

ifndef TEMPLATE
	$(error [!] TEMPLATE env not defined...FAIL)
endif

ifeq ($(wildcard $(EVENT)),)
	$(error [!] '$(EVENT)' file doesn't exist)
endif

_check_dockerd_definition:
	$(info [*] Checking whether dockerd_ip $(DOCKERD_IP) exists...)

ifndef DOCKERD_IP
	$(error [!] DOCKERD_IP env not defined...FAIL $(message1))
endif

define message1
  Env not set.
    Use following command:
      $$ export DOCKERD_IP=$$(ip a show docker0 | grep inet | cut -d / -f1 | awk '{print $$2}')

endef

_clone_service_to_build:
# wildcard matches all files with agiven pattern, here all in SERVICE/build directory
ifeq ($(wildcard $(SERVICE)/build/.),)
	$(info [+] Setting permissions for files under ${SERVICE}/)
	@find ${SERVICE}/ -type f -exec chmod go+r {} \;
	$(info [+] Setting permissions for directories under ${SERVICE}/)
	@find ${SERVICE}/ -type d -exec chmod go+rx {} \;
	$(info [+] Cloning ${SERVICE} directory structure to ${SERVICE}/build)
	@rsync -av -f "+ */" -f "- *" ${SERVICE}/ ${SERVICE}/build/
	$(info [+] Cloning source files from ${SERVICE} to ${SERVICE}/build)
	# Extract all python files (i.e. the one containing the index.handler)
	@find ${SERVICE} -type f \
		-not -name "*.pyc" \
	 	-not -name "*__pycache__" \
	 	-not -name "requirements.txt" \
	 	-not -name "event.json" \
	 	-not -name "build" | cut -d '/' -f2- > .results.txt
	# Create a hard link to the extracted python files inside the build directory
	@while read line; do \
		ln -f ${SERVICE}/`basename $$line` ${SERVICE}/build/`basename $$line`; \
	done < .results.txt
	@rm -f .results.txt
	$(info [+] Copying events to ${SERVICE}/build)
	@test -d events && rsync -avz events ${SERVICE}/build/
else
	$(info [-] '$(SERVICE)' already has a development build - Ignoring cloning task...)
endif

_check_dev_definition: _check_service_definition
	$(info [*] Checking whether service $(SERVICE) development build exists...)

ifeq ($(wildcard $(SERVICE)/build/.),)
	$(warning [FIX] run 'make build SERVICE=$(SERVICE)' to create one")
	$(error [!] '$(SERVICE)' doesn't have development build)
endif

_install_deps:
	$(info [+] Installing '$(SERVICE)' dependencies...")
	@pip install pipenv
	@$(PIPENV) lock -r > requirements.txt
	@$(PIPENV) run pip install \
		--isolated \
		--disable-pip-version-check \
		-Ur requirements.txt -t ${SERVICE}/build/
	@rm -f requirements.txt

# Package application and devs together in expected zip from build
_package: _clone_service_to_build _check_service_definition _install_deps
	@$(MAKE) _zip SERVICE="${SERVICE}"

# As its name states: Zip everything up from build
_zip: _check_dev_definition
	$(info [+] Creating '$(SERVICE)' ZIP...")
	@cd ${SERVICE}/build && zip -rq -9 "$(BASE)/$(SERVICE).zip" * \
	--exclude "wheel/*" "setuptools/*" "pkg_resources/*" "pip/*" \
			  "easy_install.py" "__pycache__/*" "*.dist-info/*" "./**/__pycache__/*"
	$(info [*] Build complete: $(BASE)/$(SERVICE).zip)

_delete_log_grp:
	@scripts/delete_cloudwatch_log_groups.sh ${AWS_REGION} ${AWS_PROFILE}

define HELP_MESSAGE

	Environment variables to be aware of or to hardcode depending on your use case:

	SERVICE
		Default: not_defined
		Info: Environment variable to declare where source code for lambda is

	DOCKER
		Default: not_defined
		Info: Environment variable to declare whether Docker should be used to build (great for C-deps)

	Common usage:

	...::: Installs all required packages as defined in the pipfile :::...
	$$ make install

	...::: Spawn a virtual environment shell :::...
	$$ make shell

	...::: Cleans up the environment - Deletes Virtualenv, ZIP builds and Dev env :::...
	$$ make clean SERVICE="cloudformation/staging/services/lambda/postgres_bootstrap"

	...::: Creates local dev environment for Python hot-reloading w/ packages:::...
	$$ make build SERVICE="cloudformation/staging/services/lambda/postgres_bootstrap"

	...::: Bundles app and dependencies into a ZIP file :::...
	$$ make package SERVICE="cloudformation/staging/services/lambda/postgres_bootstrap"

	...::: Bundles app and dependencies into a ZIP file using Docker:::...
	$$ make package SERVICE="cloudformation/staging/services/lambda/postgres_bootstrap" DOCKER=1

	...::: Sync the local cloudformation files with the artifacts Bucket :::...
	$$ make templates

	...::: Build the full architecture including cluster, auxiliaries, database, and Elastic Beanstalk :::...
	$$ make cluster

	...::: Run Pytest under tests/ with pipenv :::...
	$$ make test RUN_MODE="TESTING" DOCKERD_IP="172.17.0.1"

	...::: Run Pytest manually tests/ with pipenv :::...
	$$ DOCKERD_IP="172.17.0.1" pipenv run python -m pytest tests/integration/staging/services/my_function/my_function.py -v

endef
