#!/usr/bin/env bash

main() {
  if [ "$#" -ne 2 ]; then
    error_exit "Illegal number of parameters. You must pass in the aws region and profile name."
  fi

  read -p "Please enter the AWS region [$1]? " region
  region=${region:-"$1"}

  echo Getting group names for $region...

  LOG_GROUPS=$(
    aws logs --profile "$2" describe-log-groups --output table --region "$1" |
      awk '{print $6}' |
      grep -v ^$ |
      grep -v DescribeLogGroups
  )

  echo These log groups will be deleted:
  printf "${LOG_GROUPS}\n"
  echo Total $(wc -l <<<"${LOG_GROUPS}") log groups
  echo

  while true; do
      read -p "Proceed? [yn]" yn
      case $yn in
      [Yy]*) break ;;
      [Nn]*) exit ;;
      *) echo "Please answer yes or no." ;;
      esac
  done

  for name in ${LOG_GROUPS}; do
    printf "Delete group ${name}... "
    aws logs --profile "$2" delete-log-group --log-group-name ${name} --region $region && echo OK || echo Fail
  done
}

main "${@:-}"