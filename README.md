# Metabase AWS Cloudformation

This project aims to customize the official metabase install to make it production ready.
A secure access through an OpenVPN connection should only allow e.g. team members to use
metabase.

The Elastic Beanstalk environment will come with a HA web server and will connect to an
external postgres RDS instance.

In the future, this project could be used as a base for deploying an own Metabase fork.

## Project Design

![graph1](images/metabase-aws-cloudformation.png)

## Prerequisites

For local development, the following components are required:

+ Python 3.6
+ AWS cli cloudformation

---

## Quickstart Development

### 1. Prepare ancillary resources

* Bucket for Cloudformation artifacts
* EC2 access key for maintenance access to the application servers

The current Cloudformation Master template will create the BeanStalk environment from
Version that is indicated via `Makefile` `MB_VERSION`.

### 2. Create a new EB source bundle

The source bundle contains all files that are required for ElasticBeanstalk to launch
the metabase docker container.

#### i. Update the packaged source bundle

Initially you can use the folder `metabase-aws-eb-v0.34.0` for deployment. But note that
continued deployment requires the source bundle to be build from source `metabase/metabase`.

See section [Customize and build metabase from source](#general-instructions) for more 
information.

#### ii. Push all templates to elastic beanstalk artifacts bucket

```
$ make templates

```

**NOTE:** This only pushes the initial metabase bundle.

### 3. Create the deployment

#### i. Manually create the S3 Quickstart Bucket

I.e. 
Use the AWS Console and create a private Bucket `metabase-web-BI-100-dev-cfn-eu-central-1`.
Ensure that the naming pattern matches the one suggested in the `Makefile` *templates*
section.

#### ii. Build the Lambda package

**NOTE:** All dependencies defined in the `Pipfile` will be automatically added to the build 
package.

```
$ make package SERVICE="cloudformation/staging/services/lambda/postgres_bootstrap"

```

---

**NOTE:** Currently the folder `.../postgres_bootstrap/lib` needs to be manually copied into
the `build`folder. It contains c-libraries needed for accessing the postgres DB

postgres_bootstrap
```
├── build
│   ├── bin
│   ├── ...
│   └── urllib3-1.25.8.dist-info
├── cfnresponse.py
├── dbbootstrap.py
├── __init__.py
├── lib                         <-- MANUALLY COPY INTO build
│   ├── libpq.a
│   ├── libpq.so
│   ├── libpq.so.5
│   └── libpq.so.5.10
├── pgdb.py
└── pg.py

```


#### iii. Create the Elastic Beanstalk environment

```
$ make cluster

```

Download OpenVPN config files and activate the VPN connection 
```
$ make vpn

# Clean VPN - Deactivate First
$ nmcli con delete metabase_vpn_clientuser

```

Use your network management tool and activate `metabase_vpn_clientuser.ovpn`. Verify:
```
$ curl ipinfo.io

```



## General Instructions

### 1. Customize and build metabase from source

#### Option 1: Use the existing 'aws-eb-docker' directory

**NOTE:** The way of rebundling the official metabase source bundle is not formally 
documented, however it might be able to replicate it by following the internal in-line 
comments. 

Create custom SourceBundle from [https://github.com/metabase/metabase/tree/master/bin/aws-eb-docker](https://github.com/metabase/metabase/tree/master/bin/aws-eb-docker). Use `metabase-aws-eb-initial` to align folder structure and
required content.

Add additional config files similar to `01_metabase.config`. Those files will be executed 
prior to the EB environment creation.

#### Option 2: Create a cfn-init lambda function

For this option two things need to be provided:
1. A new metabase source bundle artifacts bucket from which the deployment can savely pull from.
2. A lambda function with a wait-handle that executes before the EB Environment and EB Application.

The Lambda function can then generate custom `*.config` files and place it into the 
`aws-eb-docker.zip` file.


## FAQ

**How does the metabase application connect to the database?**

There are two options for integrating the app with the database:
1. Having the database added inside the EB environment
2. Connect to an external database cluster

When adding a database instance to your environment, Elastic Beanstalk provides 
connection information to your application by setting environment properties for the 
database hostname, port, user name, password, and database name.

An external RDS is recommended for production. Metabase connects to external databases via
```
- Namespace: 'aws:elasticbeanstalk:application:environment'
  OptionName: "MB_DB_TYPE"
  Value: 'postgres'
- Namespace: 'aws:elasticbeanstalk:application:environment'
  OptionName: "MB_DB_NAME"
  Value: 'metabase'
- Namespace: 'aws:elasticbeanstalk:application:environment'
  OptionName: "MB_DB_PORT"
  Value: '5432'
- Namespace: 'aws:elasticbeanstalk:application:environment'
  OptionName: "MB_DB_USER"
  Value: <Admin User>
- Namespace: 'aws:elasticbeanstalk:application:environment'
  OptionName: "MB_DB_PASS"
  Value: <Master DB Password>


```

### Useful resources

- [https://github.com/metabase/metabase](https://github.com/metabase/metabase)

- [https://github.com/vanderbilt-redcap/redcap-aws-cloudformation](https://github.com/vanderbilt-redcap/redcap-aws-cloudformation)

- [https://gist.github.com/tony-gutierrez/198988c34e020af0192bab543d35a62a](https://gist.github.com/tony-gutierrez/198988c34e020af0192bab543d35a62a)

- [https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-rdsdbinstance](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-rdsdbinstance)

- [https://aws.amazon.com/blogs/database/deploy-an-amazon-aurora-postgresql-db-cluster-with-recommended-best-practices-using-aws-cloudformation/](https://aws.amazon.com/blogs/database/deploy-an-amazon-aurora-postgresql-db-cluster-with-recommended-best-practices-using-aws-cloudformation/)

- [https://github.com/aws-samples/aws-aurora-cloudformation-samples](https://github.com/aws-samples/aws-aurora-cloudformation-samples)

- [https://github.com/dacort/metabase-athena-driver](https://github.com/dacort/metabase-athena-driver)

- [https://discourse.metabase.com/t/installing-plugins-oracle-driver-on-an-aws-eb-instance/5539](https://discourse.metabase.com/t/installing-plugins-oracle-driver-on-an-aws-eb-instance/5539)

- [https://aws.amazon.com/blogs/database/iam-role-based-authentication-to-amazon-aurora-from-serverless-applications/](https://aws.amazon.com/blogs/database/iam-role-based-authentication-to-amazon-aurora-from-serverless-applications/)


## Where to go from here?

- **NEXT: Import Google Client ID and Secret as Beanstalk environment variable as GOOGLE_CLIENT_ID and GOOGLE_CLIENT_SECRET on order to enable Google OAuth**

- Replace password with IAM user access for RDS postgres

- Simulate and document a database rollback

- Replace email alerts with e.g. Slack Channel

- Implement internal SSL enrypted network traffic (443) for the user requests

- Implement the fully recommended best-practice postgres aurora cluster as indicates above, and optimize the vpn bastion host

- Fork the metabase source code and integrate it via a deployment pipeline, add the custom Docker build to an internal Docker repository


## Author

**OlafMarangone** - *Initial work* - [GitLab](https://gitlab.com/olmax99/metabase-custom-aws/-/tree/master)  
contact: olmighty99@gmail.com


