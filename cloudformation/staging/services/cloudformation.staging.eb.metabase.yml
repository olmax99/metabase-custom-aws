AWSTemplateFormatVersion: 2010-09-09
Description: >-
  Deploys Metabase by creating an ElasticBeanstalk application in an contained
  in a CloudFormation stack.

Parameters:

  MetabaseVersion:
    Description: Metabase Version
    Type: String
  KeyName:
    Description: >-
      Name of an existing EC2 KeyPair to enable SSH access to the AWS Elastic
      Beanstalk instance
    Type: 'AWS::EC2::KeyPair::KeyName'
  WebInstanceType:
    Description: The Type of EC2 instance to use for running Metabase
    Type: String
  WebAsgMin:
    Type: String
  WebAsgMax:
    Type: String
  VPCid:
    Description: The VPC to use for running Metabase
    Type: 'AWS::EC2::VPC::Id'
#  Subnets:
#    Description: The VPC subnet(s) to use for running Metabase
#    Type: 'List<AWS::EC2::Subnet::Id>'
  PrivateSubnetA:
    Type: 'AWS::EC2::Subnet::Id'
  PrivateSubnetB:
    Type: 'AWS::EC2::Subnet::Id'
  EBEndpoint:
    Description: >-
      The unique name to use for your Elastic Beanstalk URL (will be rendered
      http://(EBEndpoint).(region).elasticbeanstalk.com)
    Type: String

  RDSMasterSecret:
    Type: String
  metabaseDbUser:
    Type: String
  metabaseDbHost:
    Type: String
  ProjectSlug:
    Description: >-
      The unique name to use for your Elastic Beanstalk URL (will be rendered
      http://(EBEndpoint).(region).elasticbeanstalk.com)
    Type: String
  QSS3BucketName:
    Type: String
  BundleLocation:
    Type: String

  metabaseEbAlbSG:
    Type: 'AWS::EC2::SecurityGroup::Id'
  metabaseEbAppSG:
    Type: 'AWS::EC2::SecurityGroup::Id'

Resources:

  # TODO: Define condition 'MultiAZ enabled' and create second deployment including HA-DB instance

  # ---------------------------- Multi AZ Deployment------------------------------------
  # NOTE:
  #   This Deployment is using an external postgres cluster
  # -------------------------------------------------------------------------------------

  metabaseAppMultiAZDB:
    Type: 'AWS::ElasticBeanstalk::Application'
    Properties:
      Description: Metabase Application
      ApplicationVersions:
        - VersionLabel: !Ref MetabaseVersion
          Description: Metabase Application Version
          SourceBundle:
            S3Bucket: !Ref QSS3BucketName
            S3Key: !Join
              - /
              - - !Ref BundleLocation
                - !Ref MetabaseVersion
                - metabase-aws-eb.zip

  metabaseEnvironment:
    Type: 'AWS::ElasticBeanstalk::Environment'
    Properties:
      ApplicationName: !Ref metabaseAppMultiAZDB
      Description: AWS Elastic Beanstalk Environment for Metabase
      SolutionStackName: 64bit Amazon Linux 2018.03 v2.14.2 running Docker 18.09.9-ce
      # TemplateName: MetabaseConfiguration
      VersionLabel: !Ref MetabaseVersion
      EnvironmentName: !Ref ProjectSlug
      # CNAMEPrefix: !Ref ProjectSlug
      Tier:
        Name: WebServer
        Type: Standard
        Version: ' '
      OptionSettings:
        - Namespace: 'aws:ec2:vpc'
          OptionName: VPCId
          Value: !Ref VPCid
        - Namespace: 'aws:ec2:vpc'
          OptionName: Subnets
          Value: !Join
            - ','
            - - !Ref PrivateSubnetA
              - !Ref PrivateSubnetB
        - Namespace: 'aws:ec2:vpc'
          OptionName: ELBSubnets
          Value: !Join
            - ','
            - - !Ref PrivateSubnetA
              - !Ref PrivateSubnetB
        - Namespace: 'aws:ec2:vpc'
          OptionName: AssociatePublicIpAddress
          Value: 'false'
        - Namespace: 'aws:ec2:vpc'
          OptionName: ELBScheme
          Value: 'internal'
        # ---------------------- Autoscaling------------------------------------
        - Namespace: 'aws:autoscaling:asg'
          OptionName: MinSize
          Value: !Ref WebAsgMin
        - Namespace: 'aws:autoscaling:asg'
          OptionName: MaxSize
          Value: !Ref WebAsgMax
        - Namespace: 'aws:autoscaling:launchconfiguration'
          OptionName: InstanceType
          Value: !Ref WebInstanceType
        - Namespace: 'aws:autoscaling:launchconfiguration'
          OptionName: EC2KeyName
          Value: !Ref KeyName
        - Namespace: 'aws:autoscaling:launchconfiguration'
          OptionName: IamInstanceProfile
          Value: !Ref EBInstanceProfile
        - Namespace: 'aws:autoscaling:launchconfiguration'
          OptionName: SecurityGroups
          Value: !Ref metabaseEbAppSG
        - Namespace: 'aws:autoscaling:launchconfiguration'
          OptionName: SSHSourceRestriction
          Value: 'tcp, 22, 22, 127.0.0.1/32'
        - Namespace: 'aws:autoscaling:trigger'
          OptionName: MeasureName
          Value: CPUUtilization
        - Namespace: 'aws:autoscaling:trigger'
          OptionName: Unit
          Value: Percent
        - Namespace: 'aws:autoscaling:trigger'
          OptionName: UpperThreshold
          Value: '80'
        - Namespace: 'aws:autoscaling:trigger'
          OptionName: LowerThreshold
          Value: '20'
        - Namespace: 'aws:autoscaling:updatepolicy:rollingupdate'
          OptionName: MaxBatchSize
          Value: '1'
        - Namespace: 'aws:autoscaling:updatepolicy:rollingupdate'
          OptionName: MinInstancesInService
          Value: '0'
        - Namespace: 'aws:autoscaling:updatepolicy:rollingupdate'
          OptionName: RollingUpdateEnabled
          Value: 'true'
        - Namespace: 'aws:autoscaling:updatepolicy:rollingupdate'
          OptionName: RollingUpdateType
          Value: Health
        - Namespace: 'aws:elasticbeanstalk:environment'
          OptionName: ServiceRole
          Value: !Ref EBServiceRole
        # ---------------------- metabase Postgres connection-------------------
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_TYPE"
          Value: 'postgres'
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_NAME"
          Value: 'metabase'
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_PORT"
          Value: '5432'
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_USER"
          Value: !Ref metabaseDbUser
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_PASS"
          Value: !Join ['', ['{{resolve:secretsmanager:', !Ref RDSMasterSecret, ':SecretString:password}}' ]]
        - Namespace: 'aws:elasticbeanstalk:application:environment'
          OptionName: "MB_DB_HOST"
          Value: !Ref metabaseDbHost
        # ---------------------- Load Balancer---------------------------------
        - Namespace: 'aws:elasticbeanstalk:environment'
          OptionName: EnvironmentType
          Value: 'LoadBalanced'
        - Namespace: 'aws:elasticbeanstalk:environment'
          OptionName: LoadBalancerType
          Value: 'application'
        - Namespace: 'aws:elbv2:loadbalancer'
          OptionName: SecurityGroups
          Value: !Ref metabaseEbAlbSG
        - Namespace: 'aws:elbv2:loadbalancer'
          OptionName: ManagedSecurityGroup
          Value: !Ref metabaseEbAlbSG
        - Namespace: 'aws:elbv2:listener:default'
          OptionName: ListenerEnabled
          Value: 'true'
        - Namespace: 'aws:elasticbeanstalk:environment:process:default'
          OptionName: StickinessEnabled
          Value: 'true'
        # - Namespace: 'aws:elasticbeanstalk:environment:process:https'
        #   OptionName: Port
        #   Value: '443'
        # - Namespace: 'aws:elasticbeanstalk:environment:process:https'
        #   OptionName: Protocol
        #   Value: 'HTTPS'
        # - Namespace: 'aws:elasticbeanstalk:environment:process:https'
        #   OptionName: StickinessEnabled
        #   Value: 'true'
        - Namespace: 'aws:elasticbeanstalk:healthreporting:system'
          OptionName: SystemType
          Value: 'enhanced'

  # ---------------------------- Elastic Beanstalk Instance Profile and Service Role-----

  # Elastic Beanstalk IAM Roles
  EBServiceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      Path: /
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
        - Sid: ''
          Effect: Allow
          Principal:
            Service:
            - elasticbeanstalk.amazonaws.com
          Action: 'sts:AssumeRole'
          Condition:
            StringEquals:
              'sts:ExternalId': elasticbeanstalk
      Policies:
      - PolicyName: "root-eb"
        PolicyDocument:
          Version: 2012-10-17
          Statement:
          - Effect: Allow
            Action:
            - 'elasticloadbalancing:DescribeInstanceHealth'
            - 'elasticloadbalancing:DescribeLoadBalancers'
            - 'elasticloadbalancing:DescribeTargetHealth'
            - 'ec2:DescribeInstances'
            - 'ec2:DescribeInstanceStatus'
            - 'ec2:GetConsoleOutput'
            - 'ec2:AssociateAddress'
            - 'ec2:DescribeAddresses'
            - 'ec2:DescribeSecurityGroups'
            - 'sqs:GetQueueAttributes'
            - 'sqs:GetQueueUrl'
            - 'autoscaling:DescribeAutoScalingGroups'
            - 'autoscaling:DescribeAutoScalingInstances'
            - 'autoscaling:DescribeScalingActivities'
            - 'autoscaling:DescribeNotificationConfigurations'
            Resource: '*'

  EBInstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: /
      Roles:
        - !Ref EBInstanceProfileRole

  # TODO: Restrict bucket access as indicated in Metabase Athena example policy,
  #   for more details https://github.com/dacort/metabase-athena-driver
  EBInstanceProfileRole:
    Type: 'AWS::IAM::Role'
    Properties:
      Path: /
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - 'sts:AssumeRole'
      ManagedPolicyArns:
      - "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
      Policies:
      - PolicyName: "root-eb-access"
        PolicyDocument:
          Version: 2012-10-17
          Statement:
          - Sid: BucketAccess
            Action:
            - 's3:*'
            Effect: Allow
            # Allow Access to all buckets named elasticbeanstalk-*-
            # - !Join
            #   - ''
            #   - - 'arn:aws'
            #     - ':s3:::elasticbeanstalk-*-'
            #     - !Ref 'AWS::AccountId'
            # - !Join
            #   - ''
            #   - - 'arn:aws'
            #     - ':s3:::elasticbeanstalk-*-'
            #     - !Ref 'AWS::AccountId'
            #     - /*
            # - !Join
            #   - ''
            #   - - 'arn:aws'
            #     - ':s3:::elasticbeanstalk-*-'
            #     - !Ref 'AWS::AccountId'
            #     - '-*'
            # - !Join
            #   - ''
            #   - - 'arn:aws'
            #     - ':s3:::elasticbeanstalk-*-'
            # - !Ref 'AWS::AccountId'
            # - '-*/*'
            Resource: '*'
          - Sid: MetricsAccess
            Action:
            - 'cloudwatch:PutMetricData'
            - 'cloudwatch:PutMetricAlarm'
            - 'cloudwatch:DescribeAlarms'
            - 'cloudwatch:DeleteAlarms'
            - 'logs:CreateLogStream'
            - 'logs:PutLogEvents'
            Effect: Allow
            Resource: '*'
            # - Sid: SSMAccess
            #   Action:
            #     - 'ssm:PutParameter'
            #     - 'ssm:GetParameter'
            #   Effect: Allow
            #   Resource: 'arn:aws:ssm:*:*:parameter/redcap-salt'
            # - Sid: EncryptedEBS
            #   Action:
            #     - 'ec2:CreateVolume'
            #     - 'ec2:AttachVolume'
            #     - 'ec2:ModifyInstanceAttribute'
            #     - "ec2:Describe*"
            #   Effect: Allow
            #   Resource: '*'
          - Sid: CreateNewEBVersion
            Action:
            - 'elasticbeanstalk:RetrieveEnvironmentInfo'
            - 'elasticbeanstalk:DescribeEnvironments'
            - 'elasticbeanstalk:DescribeEvents'
            - 'elasticbeanstalk:DescribeConfigurationOptions'
            - 'elasticbeanstalk:DescribeInstancesHealth'
            - 'elasticbeanstalk:DescribeApplicationVersions'
            - 'elasticbeanstalk:DescribeEnvironmentHealth'
            - 'elasticbeanstalk:DescribeApplications'
            - 'elasticbeanstalk:ListPlatformVersions'
            - 'elasticbeanstalk:DescribeEnvironmentResources'
            - 'elasticbeanstalk:DescribeEnvironmentManagedActions'
            - 'elasticbeanstalk:RequestEnvironmentInfo'
            - 'elasticbeanstalk:DescribeEnvironmentManagedActionHistory'
            - 'elasticbeanstalk:CreateApplicationVersion'
            - 'elasticbeanstalk:ValidateConfigurationSettings'
            - 'elasticbeanstalk:DescribeConfigurationSettings'
            - 'elasticbeanstalk:CheckDNSAvailability'
            - 'elasticbeanstalk:ListAvailableSolutionStacks'
            - 'elasticbeanstalk:DescribePlatformVersion'
            - 'elasticbeanstalk:UpdateEnvironment'
            - 'cloudformation:GetTemplate'
            - 'cloudformation:DescribeStackResources'
            - 'cloudformation:DescribeStackResource'
            - 'autoscaling:DescribeAutoScalingGroups'
            - 'autoscaling:SuspendProcesses'
            - 'autoscaling:DescribeScalingActivities'
            - 'autoscaling:ResumeProcesses'
            - 'elasticloadbalancing:DescribeTargetGroups'
            - 'elasticloadbalancing:RegisterTargets'
            Effect: Allow
            Resource: '*'
          - Sid: AthenaFullAccess
            Action:
            - 'athena:*'
            - 'glue:CreateDatabase'
            - 'glue:DeleteDatabase'
            - 'glue:GetDatabase'
            - 'glue:GetDatabases'
            - 'glue:UpdateDatabase'
            - 'glue:CreateTable'
            - 'glue:DeleteTable'
            - 'glue:BatchDeleteTable'
            - 'glue:UpdateTable'
            - 'glue:GetTable'
            - 'glue:GetTables'
            - 'glue:BatchCreatePartition'
            - 'glue:CreatePartition'
            - 'glue:DeletePartition'
            - 'glue:BatchDeletePartition'
            - 'glue:UpdatePartition'
            - 'glue:GetPartition'
            - 'glue:GetPartitions'
            - 'glue:BatchGetPartition'
            Effect: Allow
            Resource: '*'

Outputs:
  URL:
    Description: Metabase URL
    Value: !GetAtt
      - metabaseEnvironment
      - EndpointURL
