import boto3
import cfnresponse
import logging
import os
import sys
# import DB-API 2.0 compliant module for PygreSQL 
from pgdb import connect
from botocore.exceptions import ClientError
import json

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DBHOST = os.environ['DBHost']
DBPORT = os.environ['DBPort']
DBNAME = os.environ['DBName']
DBUSER = os.environ['DBUser']
SECRET_ARN = os.environ['Secret_ARN']
REGION_NAME = os.environ['Region_Name']


# Here cfnresponse.SUCCESS is sent even when the operations fail as we dont want the whole cloudformation stack to
# rollback because the bootstrap of the DB failed
# You can always inspect(using cloudwatch log stream assosiated with the lambda function) why the bootstrap SQLs failed
# and manually run the SQLs to bootstrap the newly created Database
def lambda_handler(event, context):
    logger.info('## ENVIRONMENT VARIABLES')
    logger.info(os.environ)
    logger.info('## EVENT')
    logger.info(f"{event}")

    response_data = {}
    try:
        try:
            DBPASS = get_secret(SECRET_ARN, REGION_NAME)
            # Connection to SSL enabled Aurora PG database using RDS root certificate
            HOSTPORT = DBHOST + ':' + str(DBPORT)
            # For SSL add arguments: sslmode='require', sslrootcert = 'rds-combined-ca-bundle.pem'
            my_connection = connect(database=DBNAME, host=HOSTPORT, user=DBUSER, password=DBPASS)
            logger.info("SUCCESS: Connection to RDS PG instance succeeded")
      
        except Exception as e:
            logger.error('Exception: ' + str(e))
            logger.error("ERROR: Unexpected error: Couldn't connect to RDS PostgreSQL instance.")
            response_data['Data'] = "ERROR: Unexpected error: Couldn't connect to RDS PostgreSQL instance."
            cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, "None")
            sys.exit()

        # TODO: create a user with login privileges and grant IAM role access to the user (rds_iam)
        if event['RequestType'] == 'Create':
            try:
                with my_connection.cursor() as cur:
                    # Execute bootstrap SQLs
                    # pg_stat_statements: see https://www.postgresql.org/docs/11/pgstatstatements.html for more details
                    # pgaudit: see https://github.com/pgaudit/pgaudit/blob/master/README.md
                    cur.execute("create extension if not exists pg_stat_statements")
                    cur.execute("create extension if not exists pgaudit")
                    my_connection.commit()
                    cur.close()
                    my_connection.close()
                    response_data['Data'] = "SUCCESS: Executed SQL statements successfully."
                    cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, "None")
            except Exception as e:
                logger.error('Exception: ' + str(e))
                response_data['Data'] = "ERROR: Exception encountered!"
                cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, "None")
        else:
            response_data['Data'] = "{} is unsupported stack operation for this lambda function.".format(event['RequestType'])
            cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, "None")
          
    except Exception as e:
        logger.error('Exception: ' + str(e))
        response_data['Data'] = str(e)
        cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, "None")


def get_secret(secret_arn, region_name):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_arn
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            logger.error("Secrets Manager can't decrypt the protected secret text using the provided KMS key")
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            logger.error("An error occurred on the server side")
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            logger.error("You provided an invalid value for a parameter")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            logger.error("You provided a parameter value that is not valid for the current state of the resource")
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            logger.error("We can't find the resource that you asked for")
    else:
        # Decrypts secret using the associated KMS CMK.
        secret = json.loads(get_secret_value_response['SecretString'])['password']
        return secret
