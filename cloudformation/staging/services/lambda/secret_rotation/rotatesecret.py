import os
import cfnresponse
import boto3
from botocore.vendored import requests

AWS_REGION = os.environ['region']
ACCOUNT_ID = os.environ['accountId']


def lambda_handler(event, context):
    slrepoclient = boto3.client('serverlessrepo')
    cfclient = boto3.client('cloudformation')
    lambdaclient = boto3.client('lambda')
    SecretsManagerEndpoint = event['ResourceProperties']['SecretsManagerEndpoint']
    SecretRotationLambdaFnName = event['ResourceProperties']['SecretRotationLambdaFnName']
    SecretRotationLambdaStackName = event['ResourceProperties']['SecretRotationLambdaStackName']
    SubnetIds = event['ResourceProperties']['SubnetIds']
    SecretRotationLambdaSG = event['ResourceProperties']['SecretRotationLambdaSG']

    responseData = {}

    try:
        if event['RequestType'] == 'Create':
            serverlessreporesponse = slrepoclient.create_cloud_formation_change_set(
                ApplicationId=f'arn:aws:serverlessrepo:{AWS_REGION}:{ACCOUNT_ID}:applications/SecretsManagerRDSPostgreSQLRotationSingleUser',
                Capabilities=['CAPABILITY_IAM', 'CAPABILITY_RESOURCE_POLICY'],
                ParameterOverrides=[
                    {
                      'Name': 'endpoint',
                      'Value': SecretsManagerEndpoint
                    },
                    {
                      'Name': 'functionName',
                      'Value': SecretRotationLambdaFnName
                    },
                ],
                StackName=SecretRotationLambdaStackName
            )
            waiter = cfclient.get_waiter('change_set_create_complete')
            waiter.wait(
                ChangeSetName=serverlessreporesponse['ChangeSetId'],
                WaiterConfig={'Delay': 10, 'MaxAttempts': 60}
            )

            cloudformationresponse = cfclient.execute_change_set(ChangeSetName=serverlessreporesponse['ChangeSetId'])
            waiter = cfclient.get_waiter('stack_create_complete')
            waiter.wait(StackName=serverlessreporesponse['StackId'], WaiterConfig={'Delay': 10, 'MaxAttempts': 60})

            lambdaresponse = lambdaclient.add_permission(
                FunctionName=SecretRotationLambdaFnName,
                StatementId='SecretsManagerAccess',
                Action='lambda:InvokeFunction',
                Principal='secretsmanager.amazonaws.com'
            )
            lambdaresponse = lambdaclient.update_function_configuration(
                FunctionName=SecretRotationLambdaFnName,
                VpcConfig={'SubnetIds': SubnetIds, 'SecurityGroupIds': SecretRotationLambdaSG}
            )

            responseData['Data'] = "SUCCESS: Secret Rotation Lambda created successfully."
            responseData['SecretRotationLambdaARN'] = lambdaclient.get_function(FunctionName=SecretRotationLambdaFnName)['Configuration']['FunctionArn']

            cfnresponse.send(event, context, cfnresponse.SUCCESS, responseData, "None")

        elif event['RequestType'] == 'Delete':
            SecretRotationLambdaStackName = 'serverlessrepo-' + SecretRotationLambdaStackName
            response = cfclient.delete_stack(StackName=SecretRotationLambdaStackName)
            waiter = cfclient.get_waiter('stack_delete_complete')
            waiter.wait(StackName='string', WaiterConfig={'Delay': 10, 'MaxAttempts': 60})

            responseData['Data'] = "SUCCESS: Stack delete complete."
            cfnresponse.send(event, context, cfnresponse.SUCCESS, responseData, "None")

        else:
            responseData['Data'] = "{} is unsupported stack operation for this lambda function.".format(event['RequestType'])
            cfnresponse.send(event, context, cfnresponse.SUCCESS, responseData, "None")

    except Exception as e:
        print(e)
        responseData['Data'] = "ERROR: Exception encountered!"
        cfnresponse.send(event, context, cfnresponse.SUCCESS, responseData, "None")
